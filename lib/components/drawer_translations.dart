
import 'package:flutter/material.dart';
import 'package:i18n_extension/i18n_widget.dart';
import 'package:untitled1/localizations/i18n.dart';
import 'package:untitled1/pages/settings_page.dart';
import 'package:untitled1/pages/steps.dart';


class DrawerTranslations extends StatefulWidget {
  DrawerTranslations({
    Key? key,
  }) : super(key: key);

  @override
  State<DrawerTranslations> createState() => _DrawerTranslationsState();
}

class _DrawerTranslationsState extends State<DrawerTranslations> {
  var items = ["EN","ES","CA"];

  String _dropDownValue = "EN";

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
          children:[
            ListTile(
              title:Text("Settings".i18n),
              onTap: (){
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (_)=>SettingsPage()
                ));
              },

            ),
            ListTile(
              title:Text("Steps to translate".i18n),
              onTap: (){
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (_)=>
                        StepsTranslation()
                ));
              },
            ),


          ]
      ) ,);
  }
}