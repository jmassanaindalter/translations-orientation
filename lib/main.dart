import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:i18n_extension/i18n_widget.dart';
import 'package:untitled1/localizations/i18n.dart';
import 'package:untitled1/pages/settings_page.dart';

Future<void> main() async {

  WidgetsFlutterBinding.ensureInitialized();

  await MyI18n.loadTranslations();

  runApp(I18n(
      child:MyApp()
  ));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate
      ],
      supportedLocales: [
        const Locale('en'),
        const Locale('es'),
        const Locale('ca')
      ],

      home: SettingsPage(),
    );
  }
}


