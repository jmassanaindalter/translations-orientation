

import 'package:flutter/material.dart';
import 'package:i18n_extension/i18n_widget.dart';
import 'package:untitled1/components/drawer_translations.dart';
import 'package:untitled1/localizations/i18n.dart';
import 'package:untitled1/pages/settings_page.dart';

class StepsTranslation extends StatelessWidget {
  const StepsTranslation({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer:DrawerTranslations(),
      appBar: AppBar(title:Text("Steps to translate".i18n)),
      body: Container(
        child:ListView(
          children: [
            ListTile(
                title:Text("1.Import 18n_extension plugin.".i18n)),
            ListTile(
                title:Text("2.Create class localizations/i18n.dart".i18n)),
            ListTile(
                title:Text("3.Create texts with suffix .i18n "
                "and extract with command 'flutter pub run i18n_extension:getstrings' in console".i18n)),
            ListTile(
                title:Text("4.From generated strings.pot create corresponding .po files ('ca.po', 'es.po','en.po') with software POEDIT".i18n)),
            ListTile(
                title:Text("5. Add Localizations fields in MaterialApp Widget".i18n)),
            ListTile(
                title:Text("6.Call loadTranslations from i18n.dart in main".i18n)),
            ListTile(
                title:Text("7. Set language with I18n.of(context).locale = Locale('es');".i18n)),


          ],
        )
      ),
    );
  }
}

