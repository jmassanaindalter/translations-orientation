
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:i18n_extension/i18n_widget.dart';
import 'package:untitled1/components/drawer_translations.dart';
import 'package:untitled1/localizations/i18n.dart';
import 'package:untitled1/pages/user_account.dart';

class SettingsPage extends StatefulWidget {
  const SettingsPage({Key? key}) : super(key: key);

  @override
  State<SettingsPage> createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {

  var items = ["ES","EN","CA"];
  String _dropdownValue = "ES";
  bool _showUserAccount = false;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer:DrawerTranslations(),
      appBar: AppBar(title: Text("Settings".i18n),),
      //THIS ORIENTATION BUILDER ONLY WORKS OPTIMALLY IN SCAFFOLD BODY
      body: OrientationBuilder(
        builder:(context,orientation){
          log("THE ORIENTATION IS: $orientation");

          if(orientation == Orientation.portrait){
            _showUserAccount = false;
          }
          return Container(
              child:Row(
                children: [

                  Flexible(
                    child: ListView(
                        children:[

                          DropdownButton(
                              value: I18n.of(context).locale.languageCode.toUpperCase(),
                              icon: const Icon(Icons.arrow_downward),
                              onChanged: (String? newValue){
                                setState(() {
                                  _dropdownValue = newValue!;
                                  I18n.of(context).locale = Locale(newValue.toLowerCase(),"");
                                });
                              },
                              items: items.map(
                                      (String value)=>DropdownMenuItem(
                                      value: value,
                                      child: Text(value))).toList()

                          ),


                          ListTile(
                              onTap:(){

                                if(orientation == Orientation.portrait) {
                                  Navigator.of(context).push(
                                      MaterialPageRoute(builder: (_) =>
                                          Scaffold(
                                              appBar: AppBar(
                                                title: Text("User Account"),),
                                              body: UserAccountTile()))
                                  );
                                }else{
                                  setState(() {
                                    _showUserAccount = true;
                                  });
                                }
                              },
                              title:Text("User Account".i18n)),
                          ListTile(title:Text("About Application".i18n)),
                          ListTile(title:Text("Game Settings".i18n)),
                        ]
                    ),
                  ),
                  if(_showUserAccount  && orientation == Orientation.landscape)
                  Flexible(
                    child: UserAccountTile()
                  )
                ],
              )
          );
        }
      )

      ,
    );
  }
}
